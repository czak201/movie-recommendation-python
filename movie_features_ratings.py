import tensorrec
import lightfm as lf
from lightfm.evaluation import precision_at_k
from lightfm.evaluation import auc_score
import numpy
import matplotlib.pyplot as plt
import scipy
from scipy import sparse
import pandas as pd
import os
import csv
import math
import pprint

train_data_movies_path = "C:/Users/Mati/Desktop/ml-latest-small/movies.csv"
train_data_ratings_path = "C:/Users/Mati/Desktop/ml-latest-small/ratings.csv"
train_data_tags_path = "C:/Users/Mati/Desktop/ml-latest-small/tags.csv"
train_data_links_path = "C:/Users/Mati/Desktop/ml-latest-small/links.csv"
train_data_out_path = "C:/Users/Mati/Desktop/out.csv"


def load_csv(path):
    pwd = os.getcwd()
    os.chdir(os.path.dirname(path))
    train_data = pd.read_csv(os.path.basename(path), quotechar=",", quoting=csv.QUOTE_NONE,
                             low_memory=False, error_bad_lines=False)
    os.chdir(pwd)
    return train_data


#dataTags = load_csv(train_data_tags_path)
dataRatings = load_csv(train_data_ratings_path)
dataMovies = load_csv(train_data_movies_path)
#dataLinks = load_csv(train_data_links_path)
#dataOut = load_csv(train_data_out_path)
#print(dataMovies.genres)

best_ratings = dataRatings.groupby('movieId').mean()
popular_movies = dataRatings.groupby('movieId').count()
print(best_ratings["rating"])
print(popular_movies["rating"])
#print(best_ratings['rating'])

cols = ["Country"]
for x in dataMovies.genres:
    a = x.split("|")
    for index, word in enumerate(a):
        if word not in cols:
            cols.append(word)


trix = pd.DataFrame("0",columns=cols,index=numpy.unique(dataMovies.movieId))
trix.insert(0,"Mean Rating",best_ratings["rating"])
trix.insert(0,"Rating Count",popular_movies["rating"])

trix.to_csv("Movie_Ratings.csv", sep=',', encoding='utf-8')


print(trix)