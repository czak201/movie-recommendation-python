#from lightfm import LightFM  as lf
import lightfm as lf
from lightfm.datasets import fetch_movielens
from lightfm.evaluation import precision_at_k

import pandas as pd
import os as os

import csv
import numpy

from scipy import sparse

from os import walk

import sys
#print (sys.argv)

from FilmyCat import ocena
from  FilmyCat import losoweFilmy
from  FilmyCat import losoweFilmyPoGat

train_data_googleMatR_path =  os.getcwd() + "/UpdatedOut.csv"
category_data_path = os.getcwd() + "/movie_features.csv"

train_data_out_path = os.getcwd()+"/out.csv"



def load_csv(path):
    pwd = os.getcwd()
    os.chdir(os.path.dirname(path))
    train_data = pd.read_csv(os.path.basename(path), quotechar="\"", quoting=csv.QUOTE_ALL, doublequote=True,
                             #low_memory=False,
                             error_bad_lines=False,
                             encoding = 'utf-8', delimiter=",",
                             engine='python'
                             )
    os.chdir(pwd)
    return train_data

dataCategory = load_csv(category_data_path)

dataGoolgeMAtR = load_csv(train_data_googleMatR_path)
#print(dataGoolgeMAtR)
dataGoolgeMAtR = dataGoolgeMAtR.drop(dataGoolgeMAtR.columns[[0]], axis=1)  #
dataGoolgeMAtR.fillna(0,inplace=True)

dataGoolgeMAtRNOTCOO = dataGoolgeMAtR
dataGoolgeMAtR = sparse.coo_matrix(dataGoolgeMAtR)

def resolveMovTitle(  indeks):
    for row in dataCategory.iterrows():
        if row[1]['Unnamed: 0'] == indeks:
            return str(row[1]['Title'])


columy = list(dataGoolgeMAtRNOTCOO)


def printujTitles(w):
    for  i, row in enumerate(w):
        #print(row)
        #print(columy[row])
        c =  resolveMovTitle(int (columy[row]))
        print(str(i)+ " " + c)


nazwyGatunkow = list(dataCategory)
nazwyGatunkow = nazwyGatunkow[4:len(nazwyGatunkow)]
#print(nazwyGatunkow)
#print(nazwyGatunkow[5])

def wybierzGatunekDoOceny(n):
    for i, name in enumerate(nazwyGatunkow):
        print(name + " " + str(i))
    text = input("podaj numer wybranego gatunku filmowego: " )
    numer = int(text)
    films2Vote4a = losoweFilmyPoGat(nazwyGatunkow[numer], dataCategory, 10, n)
    return  films2Vote4a


#print(wybierzGatunekDoOceny())

col_names =  ['title', 'movieId', 'userRating']
df = pd.DataFrame(columns=col_names)
imie = ""

#liczba filmow, nazwa kategorii
def ocena2(filmyId):
    temp_list = []
    #temp_list.append("one")
    print("oceny proszę podawać jako jedną z puli: 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5 : ")
    #text = input("podaj swoje imie: " )
    #imie = str(text)

    for indeks, x in enumerate(filmyId):
        print(indeks)
        text = input("podaj swoją ocenę filmu: " + str(resolveMovTitle(x)) + " : ")
        numer = float(text)/5
        #listaOcenFilmow.append(numer)
        ind = df.shape[0] + 1
        #listaOcenFilmowDoCSV.append(listaFilmow[indeks] + ', ' + str(listaIdFilmow[indeks]) + ', ' + str(listaOcenFilmow[indeks]) )
        df.at[ind, 'title'] = resolveMovTitle(x)
        df.at[ind, 'movieId'] = x
        df.at[ind, 'userRating'] = numer

    return df

#ocena2(wybierzGatunekDoOceny(5))
#df.to_csv(os.getcwd() +"/Oceny/" + imie + "223.csv", sep=',', encoding='utf-8')

text = input("podaj swoje imie: " )
imie = str(text)


text = input("ile gatunkow filmów raczysz ocenić?: " )
noGat = int(text)

text = input("ile filmów raczysz ocenić?: " )
no = int(text)
for i in range(noGat):
    ocena2(wybierzGatunekDoOceny(no))
df.to_csv(os.getcwd() + "/Oceny/" + "000" +  imie + ".csv", sep=',', encoding='utf-8')


fileNames = []
for (dirpath, dirnames, filenames) in walk(os.getcwd()+"/Oceny/"):
    fileNames.extend(filenames)
    break

dataOut = load_csv(train_data_out_path)
dataOut = dataOut.drop(dataOut.columns[[0]], 1)

def insert_ratings(oceny_path):
    dataOceny = load_csv(oceny_path)
    dataOceny = dataOceny.drop(dataOceny.columns[[0]], 1)
    buckets = [0] * dataOut.shape[1]
    indeks = dataOut.shape[0]
    dataOut.loc[indeks] = buckets
    for row in dataOceny.iterrows():
        a = row[1]["userRating"]
        print(a, row[1]["movieId"])
        dataOut.at[indeks,str(row[1]["movieId"])] = float(a)
    print(dataOut)

for name in fileNames:
    print(os.getcwd()+"/Oceny/"+name)
    insert_ratings(os.getcwd()+"/Oceny/"+name)


dataOut.fillna(0, inplace=True)
df = sparse.coo_matrix(dataOut)

print(dataOut)
print(df)

model = lf.LightFM(learning_rate=0.05, loss='warp')
model.fit(df, epochs=1)

def user_prediction(userId):
    users=[userId]
    items=numpy.arange(dataOut.shape[1])
    prediction = model.predict(users,items)
    #print(prediction)
    tier = numpy.array([ numpy.arange(dataOut.shape[1]),prediction])
    #print(tier)
#prediction= numpy.sort(tier)
#qwe = numpy.sort(tier)
    qwe = numpy.argsort(prediction)
    print("Predicted movies for user : "+str(userId))
    print(qwe[dataOut.shape[1]-20:dataOut.shape[1]])


# Userzy = list(range(671,682))
# for user in Userzy:
#     user_prediction(user)

user_prediction(672)

dataOut.to_csv( "NewNewNewOut.csv", sep=',', encoding='utf-8')


#train_data_UpdatedOut_path = os.getcwd()+"/NewNewNewOut.csv"

dataUpdatedOut = dataOut

dataUpdatedOut.fillna(0, inplace=True)

uodf = sparse.coo_matrix(dataUpdatedOut)

text = input("ile epok matrix ma się Ciebie uczyć?: " )
no3 = int(text)

#Model dla UpdatedOut.csv
model = lf.LightFM(learning_rate=0.05, loss='warp')
model.fit(uodf, epochs=no3)


columy = list(dataUpdatedOut)

category_list = list(dataCategory)#[4:len(dataCategory)])
category_list = category_list[5:len(dataCategory)]

def resolveMovTitle(idex):
    categories = ""
    for row in dataCategory.iterrows():
        if row[1]["Unnamed: 0"] == idex:
            for r in category_list:
                if row[1][r] == 1:
                    categories += r + ", "
            return(str(row[1]["Title"] + " - "  + categories) )


def user_prediction(userId, topN):
    users=[userId]
    items=numpy.arange(dataUpdatedOut.shape[1])
    prediction = model.predict(users,items)
    tier = numpy.array([ numpy.arange(dataUpdatedOut.shape[1]),prediction])
    qwe = numpy.argsort(prediction)
    print("Top "+str(topN)+" najlepszych filmów dla użytkownika o ID: "+str(userId))
    forem = qwe[uodf.shape[1] - topN:uodf.shape[1]]
    for asd in forem:
        print(resolveMovTitle(int(columy[asd])))


# Userzy = list(range(dataOut.shape[0],dataUpdatedOut.shape[0]))
# for user in Userzy:
#     user_prediction(user,5)

user_prediction(672, 5)







