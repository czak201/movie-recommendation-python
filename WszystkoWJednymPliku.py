#import tensorrec
import lightfm as lf
from lightfm.evaluation import precision_at_k
from lightfm.evaluation import auc_score
import numpy
#import matplotlib.pyplot as plt
import scipy
from scipy import sparse
import pandas as pd
import os
from os import walk
import csv
import pickle
import math
import pprint


train_data_movies_path = os.getcwd()+"/ml-latest-small/movies.csv"
train_data_ratings_path = os.getcwd()+"/ml-latest-small/ratings.csv"
train_data_tags_path = os.getcwd()+"/ml-latest-small/tags.csv"
train_data_links_path = os.getcwd()+"/ml-latest-small/links.csv"
train_data_out_path = os.getcwd()+"/out.csv"
train_data_UpdatedOut_path = os.getcwd()+"/NewNewOut.csv"
category_data_path = os.getcwd() + "/movie_features.csv"


fileNames = []
for (dirpath, dirnames, filenames) in walk(os.getcwd()+"/Oceny/"):
    fileNames.extend(filenames)
    break

def load_csv(path):
    pwd = os.getcwd()
    os.chdir(os.path.dirname(path))
    train_data = pd.read_csv(os.path.basename(path), quotechar="\"",sep=',\s+', doublequote=True, quoting=csv.QUOTE_ALL, delimiter=",", error_bad_lines=False)
    os.chdir(pwd)
    return train_data



dataRatings = load_csv(train_data_ratings_path)
dataMovies = load_csv(train_data_movies_path)
dataCategory = load_csv(category_data_path)


dataOut = load_csv(train_data_out_path)
dataUpdatedOut = load_csv(train_data_UpdatedOut_path)

dataOut = dataOut.drop(dataOut.columns[[0]], 1)
dataUpdatedOut = dataUpdatedOut.drop(dataUpdatedOut.columns[[0]], 1)

dataOut.fillna(0, inplace=True)
dataUpdatedOut.fillna(0, inplace=True)

odf = sparse.coo_matrix(dataOut)
uodf = sparse.coo_matrix(dataUpdatedOut)


#Model dla UpdatedOut.csv
model = lf.LightFM(learning_rate=0.05, loss='warp')
model.fit(uodf, epochs=1)

# if not(os.path.isfile('modelUpdated')):
#     print("Tworzenie i zapisywanie modelu...")
#     model = lf.LightFM(loss='warp', learning_rate=0.05)
#     model.fit(uodf, epochs=50, num_threads=4)
#     with open('modelUpdated', 'wb') as fle:
#         pickle.dump(model, fle, protocol=pickle.HIGHEST_PROTOCOL)
#
# else:
#     print("Wczytywanie modelu...")
#     file = open('modelUpdated', 'rb')
#     model = pickle.load(file)

print(dataUpdatedOut.shape[0])
print(dataUpdatedOut.shape[1])
columy = list(dataUpdatedOut)

category_list = list(dataCategory)#[4:len(dataCategory)])
category_list = category_list[5:len(dataCategory)]

def resolveMovTitle(idex):
    categories = ""
    for row in dataCategory.iterrows():
        if row[1]["Unnamed: 0"] == idex:
            for r in category_list:
                if row[1][r] == 1:
                    categories += r + ", "
            return(str(row[1]["Title"] + " - "  + categories) )




print(resolveMovTitle(318))

def user_prediction(userId, topN):
    users=[userId]
    items=numpy.arange(dataUpdatedOut.shape[1])
    prediction = model.predict(users,items)
    tier = numpy.array([ numpy.arange(dataUpdatedOut.shape[1]),prediction])
    qwe = numpy.argsort(prediction)
    print("Top "+str(topN)+" najlepszych filmów dla użytkownika o ID: "+str(userId))
    forem = qwe[uodf.shape[1] - topN:uodf.shape[1]]
    for asd in forem:
        print(resolveMovTitle(int(columy[asd])))




Userzy = list(range(dataOut.shape[0],dataUpdatedOut.shape[0]))
for user in Userzy:
    user_prediction(user,5)