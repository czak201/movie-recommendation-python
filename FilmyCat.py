import sys

import pandas as pd
import math
import random
#from LFM4 import resolveMovTitle
import os

listaFilmow = [
    'Toy Story (1995)',
'Jumanji (1995)',
'GoldenEye (1995)',
'Mortal Kombat (1995)',
'Apollo 13 (1995)',

'From Dusk Till Dawn (1996)',
'Castle Freak (1995)',
'Beyond Bedlam (1993)',
'Village of the Damned (1995)',
'Wolf (1994)',

'Casino (1995)',
'Othello (1995)',
'Dangerous Minds (1995)',
'Beauty of the Day (Belle de jour) (1967)',
'Fearless (1993)',



'Ed (1996)',
'Sgt. Bilko (1996)',
'Angus (1995)',
'Space Jam (1996)',
'High School High (1996)',
'Breakdown (1997)',
'Con Air (1997)',
'Air Force One (1997)',
'Jackal, The (1997)',
'Hush (1998)',
'Yards, The (2000)',
'Coogan\'s Bluff (1968)',
'True Believer (1989)',
'Enforcer, The (1976)',
'American Crime, An (2007)',
'Nico Icon (1995)',
'Catwalk (1996)',
'War Room, The (1993)',
'Snowriders (1996)',
'Everest (1998)']


listaIdFilmow = [1,2,10,44,150,
                 70,220,285,332,382,
                 16,26,31,154,448,
                 619,637,700,
                 673,833,1518,1552,
                 1608,1687,1798,2769,
                 3427,4279,4945,59418,
                 77,108,556,1145,1797]

listaFilmowLen = len(listaFilmow)
listaOcenFilmow = []
listaOcenFilmowDoCSV = []
col_names =  ['title', 'movieId', 'userRating']
df = pd.DataFrame(columns=col_names)#pd.DataFrame(index=range(listaFilmowLen), columns=3)

def ocena():
    temp_list = []
    #temp_list.append("one")

    for indeks, x in enumerate(listaFilmow):
        text = input("podaj swoją ocenę filmu: " + listaFilmow[indeks] + " : ")
        numer = float(text)
        listaOcenFilmow.append(numer)
        #listaOcenFilmowDoCSV.append(listaFilmow[indeks] + ', ' + str(listaIdFilmow[indeks]) + ', ' + str(listaOcenFilmow[indeks]) )
        df.at[indeks, 'title'] = listaFilmow[indeks]
        df.at[indeks, 'movieId'] = listaIdFilmow[indeks]
        df.at[indeks, 'userRating'] = listaOcenFilmow[indeks]

        #df.append(df2)
        #listaOcenFilmowDoCSV.to_csv("C:/Downloads/coursera/itdevrelop/oceny.csv", sep=',', encoding='utf-8')
    #cs = pd.DataFrame(listaOcenFilmowDoCSV)
    #print(cs)
    df.to_csv("C:/Downloads/coursera/itdevrelop/oceny2.csv", sep=',', encoding='utf-8')
    return df

#liczba filmow, nazwa kategorii
# def ocena2(filmyId):
#     temp_list = []
#     #temp_list.append("one")
#     text = input("podaj swoje imie: " )
#     imie = str(text)
#     for indeks, x in enumerate(filmyId):
#         text = input("podaj swoją ocenę filmu: " + resolveMovTitle(indeks) + " : ")
#         numer = float(text)
#         #listaOcenFilmow.append(numer)
#         #listaOcenFilmowDoCSV.append(listaFilmow[indeks] + ', ' + str(listaIdFilmow[indeks]) + ', ' + str(listaOcenFilmow[indeks]) )
#         df.at[indeks, 'title'] = resolveMovTitle(indeks)
#         df.at[indeks, 'movieId'] = indeks
#         df.at[indeks, 'userRating'] = numer
#     df.to_csv(os.getcwd() +"/Oceny/" + imie + "223.csv", sep=',', encoding='utf-8')
#     return df

#df2 = ocena()

#print(df)
#print(df2)

#Unnamed: 0; Title; NoOfRatings; MeanOfRatings; Categorie
#losowe Filmy Ocenione Przez >10 Osob
# input pandas frame
# treshold = min liczba filmow juz oceniona
# n = liczba filmow na out
def losoweFilmy(dane, treshold, n):
    pulaFilmow = []
    for row in dane.iterrows():
        if   not (math.isnan(row[1]['NoOfRatings']))  & (row[1]['NoOfRatings'] >  treshold):
            pulaFilmow.append(row[1]['Unnamed: 0'])
    for row in range(n):
        return random.sample(pulaFilmow, n)
    #random.choice(pulaFilmow)

def losoweFilmyPoGat(gatunek, dane, treshold, n,yearFrom,yearTo):
    pulaFilmow = []
    for row in dane.iterrows():
        if (not (math.isnan(row[1]['NoOfRatings'])))  & (row[1]['NoOfRatings'] <=  treshold) & (row[1][gatunek] == 1) & (row[1]['Year'] >= yearFrom) & (row[1]['Year'] <= yearTo):
            pulaFilmow.append(row[1]['Unnamed: 0'])
    nowyN = 0
    if len(pulaFilmow) <= n:
        nowyN = len(pulaFilmow)
    else:
        nowyN = n
    for row in range(nowyN):
        return random.sample(pulaFilmow, nowyN), pulaFilmow

def dajFilmyZKat(dane, name,yearFrom,yearTo):
    pulaFilmow = []
    for row in dane.iterrows():
        if (not (math.isnan(row[1]['NoOfRatings']))) &  (row[1][name] == 1) \
                & (row[1]['Year'] >= yearFrom) & (row[1]['Year'] <= yearTo):
            pulaFilmow.append(row[1]['Unnamed: 0'])
    return pulaFilmow

def znajdzNoMov(rPredikts, filmyCalaKat, notIn):

    #print(rPredikts)
    #print(len(rPredikts))#850 #numer kolumny
    #print(len(filmyCalaKat))#50 #numer kolumny - prawie na pewno
    for j, row in enumerate(rPredikts):
        #print(row)
        for k, r1 in enumerate(filmyCalaKat):#filmy kat od nowa zrobic
            #print(r1)
            if ((row == r1) & (  not r1 in notIn)):
                return r1



def topNFilmyZGat(predikts, dane, catName, topN,yearFrom,yearTo):
    topMovi = []
    notIN = []
    #print(predikts)
    revPredikts = predikts[::-1]
    filmyZKat = dajFilmyZKat(dane, catName,yearFrom,yearTo)
    nowyN = 0
    if len(filmyZKat) <= topN:
        nowyN = len(filmyZKat)
    else:
        nowyN = topN

    #print(nowyN)
    for i in range(nowyN):
        #print(i)
        movId = znajdzNoMov(revPredikts, filmyZKat, notIN)
        notIN.append(movId)
        topMovi.append(movId)
        #print(movId)
    return topMovi

#nazwy gatunkow
#def nazwyGetunkow():






















