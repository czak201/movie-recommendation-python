import csv
import pandas as pd
import os as os
import numpy as np
from lightfm import LightFM
from lightfm.datasets import fetch_movielens
from lightfm.evaluation import precision_at_k
from scipy.sparse import coo_matrix
import pickle
import os.path


train_data_movies_path = "C:/Users/CzaK/Downloads/movies.csv"
train_data_ratings_path = "C:/Users/CzaK/Downloads/ratings.csv"
train_data_links_path = "C:/Users/CzaK/Downloads/links.csv"
train_data_tags_path = "C:/Users/CzaK/Downloads/tags.csv"
mydata_path = "C:/Users/CzaK/PycharmProjects/test2/mydata.csv"
item_features_path = "C:/Users/CzaK/PycharmProjects/test2/item_features.csv"
oceny_path = "C:/Users/CzaK/PycharmProjects/test2/oceny.csv"


def load_csv(path):
    pwd = os.getcwd()
    os.chdir(os.path.dirname(path))
    train_data = pd.read_csv(os.path.basename(path), quotechar=",",quoting=csv.QUOTE_NONE,
                             low_memory=False, error_bad_lines=False)
    os.chdir(pwd)
    return train_data


data_movies = load_csv(train_data_movies_path)
data_ratings = load_csv(train_data_ratings_path)
data_links = load_csv(train_data_links_path)
data_tags = load_csv(train_data_tags_path)
mydata = load_csv(mydata_path)
data_item_features = load_csv(item_features_path)
data_oceny = load_csv(oceny_path)

oceny = np.array([data_oceny["Unnamed: 0"], data_oceny["0"]])
print(oceny)

# df = pd.DataFrame(0,columns = np.unique(data_ratings.movieId),index = np.unique(data_ratings.userId))
# df = df.applymap("{0:.1f}".format)
#
# for row in data_ratings.itertuples():
#     zx = getattr(row, "rating")
#     df.at[getattr(row, "userId"), getattr(row, "movieId")] = float(zx)/5.0
# #print(df)
#
# df.to_csv("mydata.csv", sep=',', encoding='utf-8')


mydata=mydata.drop(mydata.columns[[0]], 1)
#mydata.fillna(0, inplace=True)
#print(mydata)
data = coo_matrix(mydata)
#print(data)

if not(os.path.isfile('model')):
    print("Tworzenie i zapisywanie modelu...")
    model = LightFM(loss='warp', learning_rate=0.05)
    model.fit(data, epochs=20, num_threads=4)
    with open('model', 'wb') as fle:
        pickle.dump(model, fle, protocol=pickle.HIGHEST_PROTOCOL)

else:
    print("Wczytywanie modelu...")
    file = open('model', 'rb')
    model = pickle.load(file)

# test_precision = precision_at_k(model, data, k=5).mean()
# print(test_precision)

users = [671]
items = np.arange(data.shape[1])

predictions = model.predict(users, items)
predictions_with_movieId = np.array([np.arange(data.shape[1]), predictions])
sorted_predictions_with_movieId = np.argsort(predictions)
print(sorted_predictions_with_movieId[data.shape[1]-20:data.shape[1]])

#print