#import tensorrec
import lightfm as lf
from lightfm.evaluation import precision_at_k
from lightfm.evaluation import auc_score
import numpy
#import matplotlib.pyplot as plt
import scipy
from scipy import sparse
import pandas as pd
import os
from os import walk
import csv
import math
import pprint

# train_data_movies_path = "C:/Users/Mati/Desktop/ml-latest-small/movies.csv"
# train_data_ratings_path = "C:/Users/Mati/Desktop/ml-latest-small/ratings.csv"
# train_data_tags_path = "C:/Users/Mati/Desktop/ml-latest-small/tags.csv"
# train_data_links_path = "C:/Users/Mati/Desktop/ml-latest-small/links.csv"
# train_data_ceny_path = "C:/Users/Mati/Desktop/oceny.csv"
train_data_out_path = os.getcwd()+"/out.csv"
#oceny_path = os.getcwd()+"/Oceny/piotr.csv"

fileNames = []
for (dirpath, dirnames, filenames) in walk(os.getcwd()+"/Oceny/"):
    fileNames.extend(filenames)
    break

def load_csv(path):
    pwd = os.getcwd()
    os.chdir(os.path.dirname(path))
    train_data = pd.read_csv(os.path.basename(path), quotechar=",", quoting=csv.QUOTE_NONE,
                             low_memory=False, error_bad_lines=False)
    os.chdir(pwd)
    return train_data

dataOut = load_csv(train_data_out_path)
dataOut = dataOut.drop(dataOut.columns[[0]], 1)
print(dataOut)


# dataOceny = load_csv(oceny_path)
# print(dataOceny)
#
def insert_ratings(oceny_path):
    dataOceny = load_csv(oceny_path)
    dataOceny = dataOceny.drop(dataOceny.columns[[0]], 1)
    buckets = [0] * dataOut.shape[1]
    indeks = dataOut.shape[0]
    dataOut.loc[indeks] = buckets
    for row in dataOceny.iterrows():
        a = row[1]["userRating"]
        print(a, row[1]["movieId"])
        dataOut.at[indeks,str(row[1]["movieId"])] = float(a)
    print(dataOut)

for name in fileNames:
    print(os.getcwd()+"/Oceny/"+name)
    insert_ratings(os.getcwd()+"/Oceny/"+name)


dataOut.fillna(0, inplace=True)
df = sparse.coo_matrix(dataOut)

print(dataOut)
print(df)

model = lf.LightFM(learning_rate=0.05, loss='warp')
model.fit(df, epochs=50)

def user_prediction(userId):
    users=[userId]
    items=numpy.arange(dataOut.shape[1])
    prediction = model.predict(users,items)
    #print(prediction)
    tier = numpy.array([ numpy.arange(dataOut.shape[1]),prediction])
    #print(tier)
#prediction= numpy.sort(tier)
#qwe = numpy.sort(tier)
    qwe = numpy.argsort(prediction)
    print("Predicted movies for user : "+str(userId))
    print(qwe[dataOut.shape[1]-20:dataOut.shape[1]])


Userzy = list(range(671,682))
for user in Userzy:
    user_prediction(user)


dataOut.to_csv( "NewNewOut.csv", sep=',', encoding='utf-8')