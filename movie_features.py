import tensorrec
import lightfm as lf
from lightfm.evaluation import precision_at_k
from lightfm.evaluation import auc_score
import numpy
import matplotlib.pyplot as plt
import scipy
from scipy import sparse
import pandas as pd
import os
import csv
import math
import pprint

train_data_movies_path = os.getcwd()+"/ml-latest-small/movies.csv"
train_data_ratings_path = os.getcwd()+"/ml-latest-small/ratings.csv"
train_data_tags_path = os.getcwd()+"/ml-latest-small/tags.csv"
train_data_links_path = os.getcwd()+"/ml-latest-small/links.csv"
train_data_out_path = os.getcwd()+"/out.csv"
train_data_UpdatedOut_path = os.getcwd()+"/UpdatedOut.csv"
category_data_path = os.getcwd() + "/movie_features.csv"

def load_csv(path):
    pwd = os.getcwd()
    os.chdir(os.path.dirname(path))
    train_data = pd.read_csv(os.path.basename(path), quotechar="\"",sep=',\s+', doublequote=True, quoting=csv.QUOTE_ALL, delimiter=",", error_bad_lines=False)
    os.chdir(pwd)
    return train_data


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


#dataTags = load_csv(train_data_tags_path)
dataRatings = load_csv(train_data_ratings_path)
dataMovies = load_csv(train_data_movies_path)
#dataLinks = load_csv(train_data_links_path)
#dataOut = load_csv(train_data_out_path)
#print(dataMovies.genres)

print(dataMovies)

rey=["Title","Year"]
for x in dataMovies.genres:
    a = x.split("|")
    for index, word in enumerate(a):
        if word not in rey:
            rey.append(word)
print(rey)

best_ratings = dataRatings.groupby('movieId').mean()
popular_movies = dataRatings.groupby('movieId').count()


trix = pd.DataFrame("0",columns=rey,index=numpy.unique(dataMovies.movieId))
trix.insert(2,"MeanOfRatings",best_ratings["rating"])
trix.insert(2,"NoOfRatings",popular_movies["rating"])

print(trix)

for row in dataMovies.iterrows():
    #print(row)
    #trix.at[1]["MovieId"] = row[1][0]
    genres = row[1][2].split("|")
    trix.at[row[1][0],"Title"] = row[1][1]
    a = row[1][1].split("(")
    a[len(a) - 1] = a[len(a) - 1][:-1]
    if is_number(a[len(a) - 1]):
        trix.at[row[1][0], "Year"] = int(a[len(a) - 1])
    else:
        trix.at[row[1][0], "Year"] = 0
    for index, word in enumerate(genres):
        trix.at[row[1][0],word]=1

for row in trix.itertuples():
     if row.Year < 2000:
         #trix.drop(trix.row[[row.Index]], 1)
         trix = trix.drop([row.Index])

print("trix3")
print(trix)
trix.to_csv("movie_features.csv", sep=',', encoding='utf-8')
