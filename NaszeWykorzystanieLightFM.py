import pickle

import tensorrec
import lightfm as lf
from lightfm.evaluation import precision_at_k
from lightfm.evaluation import auc_score
import numpy
import matplotlib.pyplot as plt
import scipy
from scipy import sparse
import pandas as pd
import os as os
import csv
import math
import pprint

train_data_movies_path = os.getcwd()+"/ml-latest-small/movies.csv"
train_data_ratings_path = os.getcwd()+"/ml-latest-small/ratings.csv"
train_data_tags_path = os.getcwd()+"/ml-latest-small/tags.csv"
train_data_links_path = os.getcwd()+"/ml-latest-small/links.csv"
train_data_out_path = os.getcwd()+"/out.csv"
train_data_UpdatedOut_path = os.getcwd()+"/UpdatedOut.csv"
category_data_path = os.getcwd() + "/movie_features.csv"



def load_csv(path):
    pwd = os.getcwd()
    os.chdir(os.path.dirname(path))
    train_data = pd.read_csv(os.path.basename(path), quotechar=",", quoting=csv.QUOTE_NONE,
                             low_memory=False, error_bad_lines=False)
    os.chdir(pwd)
    return train_data


dataTags = load_csv(train_data_tags_path)
dataRatings = load_csv(train_data_ratings_path)
dataMovies = load_csv(train_data_movies_path)
dataLinks = load_csv(train_data_links_path)
dataOut = load_csv(train_data_out_path)

print(dataOut)
dataOut = dataOut.drop(dataOut.columns[[0]], 1)
print(dataOut)
#print(dataTags)
#print(dataRatings)
#print(dataMovies)
#print(dataLinks)

#userIds = numpy.unique(dataRatings.userId)
#columnnames = numpy.unique(dataMovies.movieId)

#m=np.matrix(np.unique(dataRatings.userId), str(np.unique(dataMovies.movieId)))
#print(a)
#print(b)

#df = pd.DataFrame(0,columns=np.unique(dataMovies.movieId),index=np.unique(dataRatings.userId))
#print(df)
#de = pd.DataFrame(dataRatings.head(5))

dataOut.fillna(0, inplace=True)
print(dataOut)
df = sparse.coo_matrix(dataOut)
print(df)
# Build the model with default parameters
# model = lf.LightFM(learning_rate=0.05, loss='warp')
# model.fit(df, epochs=50)
if not(os.path.isfile('model')):
    print("Tworzenie i zapisywanie modelu...")
    model = lf.LightFM(loss='warp', learning_rate=0.05)
    model.fit(df, epochs=50, num_threads=4)
    with open('model', 'wb') as fle:
        pickle.dump(model, fle, protocol=pickle.HIGHEST_PROTOCOL)

else:
    print("Wczytywanie modelu...")
    file = open('model', 'rb')
    model = pickle.load(file)

#
# train_precision = precision_at_k(model, df, k=10).mean()
# test_precision = precision_at_k(model, df, k=10).mean()
#
# train_auc = auc_score(model, df).mean()
# test_auc = auc_score(model, df).mean()
#
# print('Precision: train %.2f, test %.2f.' % (train_precision, test_precision))
# print('AUC: train %.2f, test %.2f.' % (train_auc, test_auc))

users=[671]
items=numpy.arange(9066)
print(items)
prediction = model.predict(users,items)
print(prediction)
tier = numpy.array([ numpy.arange(9066),prediction])
print(tier)
#prediction= numpy.sort(tier)
#qwe = numpy.sort(tier)
qwe = numpy.argsort(prediction)
print(qwe)
columy = list(dataOut)
print(columy)
forem = qwe[df.shape[1]-20:df.shape[1]]
print(forem)
for asd in forem:
    print(columy[asd])