#import tensorrec
import lightfm as lf
from lightfm.evaluation import precision_at_k
from lightfm.evaluation import auc_score
import numpy
#import matplotlib.pyplot as plt
import scipy
from scipy import sparse
import pandas as pd
import os
from os import walk
import csv
import pickle
import math
import pprint
from  FilmyCat import losoweFilmyPoGat
from FilmyCat import topNFilmyZGat
import time
import random

train_data_movies_path = os.getcwd()+"/ml-latest-small/movies.csv"
train_data_ratings_path = os.getcwd()+"/ml-latest-small/ratings.csv"
train_data_tags_path = os.getcwd()+"/ml-latest-small/tags.csv"
train_data_links_path = os.getcwd()+"/ml-latest-small/links.csv"
train_data_out_path = os.getcwd()+"/out.csv"
train_data_UpdatedOut_path = os.getcwd()+"/NewNewOut.csv"
category_data_path = os.getcwd() + "/movie_features.csv"


fileNames = []
for (dirpath, dirnames, filenames) in walk(os.getcwd()+"/Oceny/"):
    fileNames.extend(filenames)
    break

def load_csv(path):
    pwd = os.getcwd()
    os.chdir(os.path.dirname(path))
    train_data = pd.read_csv(os.path.basename(path), quotechar="\"",sep=',\s+', doublequote=True, quoting=csv.QUOTE_ALL, delimiter=",", error_bad_lines=False)
    os.chdir(pwd)
    return train_data

def resolveMovTitle(idex):
    categories = ""
    for row in dataCategory.iterrows():
        if row[1]["Unnamed: 0"] == idex:
            for r in category_list:
                if row[1][r] == 1:
                    categories += r + ", "
            return(str(row[1]["Title"] + " - "  + categories) )


def dajTopNMovZKat(qwe, topN, catName, dane,yearFrom,yearTo):
    #wyciaganie topn filmow z kategorii
    topka = topNFilmyZGat(qwe, dane, catName, topN,yearFrom,yearTo)
    #print(len(topka))
    for i, row in enumerate(topka):
        #print(i, row )
        #print( i)
        if (row is None):
            print("Nie możemy zarekomendować innych filmów w tej kategorii dla zadanych lat.")#)int(items[asd]))))
        else:
            print(str(i) + " " + resolveMovTitle(row))

#WARNING !!! hardkodowane dane !!!
def dajKatNPC(category_list):
    CEO_path = os.getcwd()+ "/Oceny/0PABLO ESCOBAR.csv"
    df_CEO = load_csv(CEO_path)
    nazwyRatedCats = []
    for row in df_CEO.itertuples():#rows():
        nazwyRatedCats.append(row.categoryName) #row[1]['categoryName'])
    return nazwyRatedCats


def user_prediction(userId, topN, dataCoo,yearFrom,yearTo, dane, czyRealUser):
    users=[userId]
    items, itemIndexes = getMovieIdByYear(yearFrom,yearTo,dataCategory)
    # print(moviesFromRange)
    # items=numpy.arange(len(moviesFromRange))
    start = time.time()
    prediction = model.predict(users,itemIndexes)
    tier = numpy.array([ itemIndexes,prediction])
    qwe = numpy.argsort(prediction)
    end = time.time()
    print("Czas wykonania prediction wynosi:")
    print(end - start)
    print("Top "+str(topN)+" najlepszych filmów dla użytkownika o ID: "+str(userId))
    forem = qwe[len(items) - topN:len(items)]
    for asd in forem:
        print(resolveMovTitle(int(items[asd])))

    nazwaKat = category_list[0]
    if not czyRealUser:
        #nazwaKat = category_list[cat]
        nazwaKat = dajKatNPC(category_list)
    #else:
        #nazwaKat = dajKatNPC(category_list, userId)

    for i, cat in enumerate(userKat):
        if czyRealUser:
            #nazwaKat = category_list[cat]
            print("Top " + str(topN) + " najlepszych filmów dla użytkownika o ID: " + str(userId) + " w kategorii: " +
                  category_list[cat])
            dajTopNMovZKat(qwe, topN, category_list[cat], dane, yearFrom, yearTo)
        else:
            #nazwaKat = dajKatNPC(category_list, userId)
            print("Top " + str(topN) + " najlepszych filmów dla użytkownika o ID: " + str(userId) + " w kategorii: " +
                  nazwaKat[i])
            dajTopNMovZKat(qwe, topN, nazwaKat[i], dane, yearFrom, yearTo)  # puleFilmowZRozKAt[i])
        #dajTopNMovZKat(qwe, topN, nazwaKat, dane,yearFrom,yearTo)#puleFilmowZRozKAt[i])

tresholdDlaLiczbyOcenionychFilmow = 50

#pulaFilmZGat = []

def wybierzGatunekDoOceny(iloscFilmow,numerGatunku,yearFrom,yearTo):
    films2Vote4a = losoweFilmyPoGat(category_list[numerGatunku], dataCategory, tresholdDlaLiczbyOcenionychFilmow, iloscFilmow, yearFrom, yearTo)
    return  films2Vote4a

def printujTitles(w):
    for  i, row in enumerate(w):
        #print(row)
        #print(columy[row])
        c =  resolveMovTitle(int (columy[row]))
        print(str(i)+ " " + c)

def insert_ratings(oceny_path):
    dataOceny = load_csv(oceny_path)
    dataOceny = dataOceny.drop(dataOceny.columns[[0]], 1)
    buckets = [0] * dataOut.shape[1]
    indeks = dataOut.shape[0]
    dataOut.loc[indeks] = buckets
    for row in dataOceny.iterrows():
        a = row[1]["userRating"]
        print(a, row[1]["movieId"])
        dataOut.at[indeks,str(row[1]["movieId"])] = float(a)
    print(dataOut)

def skipMov(filmyCalaKat):
    #for indeks, x in enumerate(filmyCalaKat):
    x = random.choice(filmyCalaKat)
    text = str(ignoreEmtyStr("Podaj swoją ocenę filmu: " + str(resolveMovTitle(x)) + " : "))
    # if (float(text) < 1):
    #     text = skipMov(filmyCalaKat)
    # else:
    #     return text
    while(float(text) < 1):
        text = skipMov(filmyCalaKat)
    return text

def ocena2(filmyId, filmyCalaKat, catName):
    print("Oceny proszę podawać z puli: 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5 : ")
    print("Aby ocenić inny film (skip) wprowadź: 0")
    for indeks, x in enumerate(filmyId):
        text = str(ignoreEmtyStr("Podaj swoją ocenę filmu: " + str(resolveMovTitle(x)) + " : "))
            #input("Podaj swoją ocenę filmu: " + str(resolveMovTitle(x)) + " : ")
        if (float(text) < 1):
            text = skipMov(filmyCalaKat)
        numer = float(text)/5
        #listaOcenFilmow.append(numer)
        newId = df.shape[0] + 1
        #listaOcenFilmowDoCSV.append(listaFilmow[indeks] + ', ' + str(listaIdFilmow[indeks]) + ', ' + str(listaOcenFilmow[indeks]) )
        df.at[newId, 'title'] = resolveMovTitle(x)
        df.at[newId, 'movieId'] = x
        df.at[newId, 'userRating'] = numer
        df.at[newId, 'categoryName'] = catName

def getMovieIdByYear(yearFrom,yearTo,dataframe):
    movieIds = []
    movieIndex = []
    for i, x in dataframe.iterrows():
        if (x[2] >= int(yearFrom)) & (x[2] <= int(yearTo)):
            movieIds.append(x[0])
            movieIndex.append(i)
    return(movieIds,movieIndex)


col_names =  ['title', 'movieId', 'userRating']
df = pd.DataFrame(columns=col_names)

dataRatings = load_csv(train_data_ratings_path)
dataMovies = load_csv(train_data_movies_path)
dataCategory = load_csv(category_data_path)

dataOut = load_csv(train_data_out_path)
dataUpdatedOut = load_csv(train_data_UpdatedOut_path)

dataOut = dataOut.drop(dataOut.columns[[0]], 1)
dataUpdatedOut = dataUpdatedOut.drop(dataUpdatedOut.columns[[0]], 1)

dataOut.fillna(0, inplace=True)
dataUpdatedOut.fillna(0, inplace=True)

dataOutCoo = sparse.coo_matrix(dataOut)
dataUpdatedOutCoo = sparse.coo_matrix(dataUpdatedOut)

columy = list(dataOut)

category_list = list(dataCategory)#[4:len(dataCategory)])
category_list = category_list[5:len(dataCategory)]

def ignoreEmtyStrINT(text):
    s = input(text)
    s.strip(' \t\n\r')
    s.strip(' abcdefghijklmnoprstquwvxyz')
    s.strip('')
    if len(s) < 1:
        ignoreEmtyStrINT(text)
    else :
        #for code in s.encode('ascii'):
         #   print(code)
        return int(float(s))

def ignoreEmtyStr(text):
    s = input(text)
    s.strip(' \t\n\r')
    if len(s) < 1:
        ignoreEmtyStr(text)
    else :
        #for code in s.encode('ascii'):
        #    print(code)
        return s

algorithms = ["warp","warp-kos","bpr"]
text = str(ignoreEmtyStr("Podaj swój pseudonim:"))#input("Podaj swój pseudonim: ")
nickname = str(text)
iterations = 0

userKat = []
movieAmount = int(str(ignoreEmtyStr("Wybierz ilość filmów do oceny:")))

puleFilmowZRozKAt = []

while True:
    df = ""
    col_names = ['title', 'movieId', 'userRating']
    df = pd.DataFrame(columns=col_names)

    minYear = 2020
    maxYear = -1
    yearFrom = 2020
    yearTo = -1
    while yearFrom > yearTo:
        print("Podaj od którego do którego roku produkcji filmy chcesz oceniac")
        text = str(ignoreEmtyStrINT("Od:") )#input("Od: ")
        yearFrom = int(text)
        text = str(ignoreEmtyStrINT("Do:"))#input("Do: ")
        yearTo = int(float(text))
    if yearFrom <= minYear:
        minYear = yearFrom
    if yearTo >= maxYear:
        maxYear = yearTo

    text = str(ignoreEmtyStrINT("Podaj ilość gatunków, które chciałbyś ocenić:"))#input("Podaj ilość gatunków, które chciałbyś ocenić: ")
    categoryAmount = int(text)

    for a in range(categoryAmount):
        for i, name in enumerate(category_list):
            print(name + " " + str(i))
        text = str(ignoreEmtyStrINT("Wybierz gatunek z listy:"))#input("Wybierz gatunek z listy: ")
        categoryChoice = int(text)
        userKat.append(categoryChoice)
        #text = input("Wybierz ilość filmów do oceny z wybranego gatunku: ")
        #movieAmount = 5
        sampleMov, pulaFilmZGat = wybierzGatunekDoOceny(movieAmount,categoryChoice,yearFrom,yearTo)
        puleFilmowZRozKAt.append(pulaFilmZGat)
        ocena2(sampleMov, pulaFilmZGat, category_list[categoryChoice])

    df.to_csv(os.getcwd() + "/Oceny/" + str(iterations) + nickname + ".csv", sep=',', encoding='utf-8')
    iterations+=1

    #dopisanie danych ocen CEO do csv bazowej
    #na indeks 672:
    #insert_ratings(os.getcwd() + "/Oceny/" + "0PABLO ESCOBAR" + ".csv")
    #dataOut.to_csv(os.getcwd() + "/out.csv")

    for i in range(iterations):
        insert_ratings(os.getcwd() + "/Oceny/" + str(i) + nickname + ".csv")



    for i, name in enumerate(algorithms):
        print(name)
    #text = input("Wybierz algorytm : ")
    print('czekaj')
    algorithm = str('warp')#str(text)
    #text = input("Podaj ilość epok: ")
    epok = 1 #int(text)
    dataOutCoo = sparse.coo_matrix(dataOut)
    model = lf.LightFM(learning_rate=0.05, loss=algorithm)
    start = time.time()
    model.fit(dataOutCoo, epochs=epok)
    end = time.time()
    print("Czas wykonania model fit wynosi:")
    print(end - start)
    lastUser = dataOut.shape[0]
    print("ID twojego uzytkownika :"+str(lastUser-1))
    user_prediction(lastUser-1, 10, dataOutCoo, minYear, maxYear, dataCategory, True)
    print()
    print("ID uzytkownika Escobar:" + str(671))
    user_prediction(671, 10, dataOutCoo, minYear, maxYear, dataCategory, False)
    print("Powrót do poczatku.")


#start = time.time()
#print("hello")
#end = time.time()
#print(end - start)



